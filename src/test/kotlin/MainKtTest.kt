import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class MainKtTest {
    private val testInput = listOf<Int>(16,1,2,0,4,2,7,1,2,14)

    @Test
    fun getSmallestFuelCost() {
        assertEquals(168, getSmallestFuelCost(testInput))
    }
}