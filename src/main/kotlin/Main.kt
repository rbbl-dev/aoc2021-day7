import java.io.File

fun main() {
    println(getSmallestFuelCost(parseInput()))
}

fun parseInput(): List<Int> = File("input.txt").readText().trim().split(",").map { it.toInt() }

fun getSmallestFuelCost(input: List<Int>): Int {
    val fuelCostsPerPoint = hashMapOf<Int, Int>()

    for (i in 0 .. input.maxOrNull()!!) {
        fuelCostsPerPoint[i] = getFuelCostForPoint(i, input)
    }

    var smallestCost: Int? = null
    fuelCostsPerPoint.values.forEach {
        if (smallestCost == null || it < smallestCost!!) {
            smallestCost = it
        }
    }

    return smallestCost!!
}

fun getFuelCostForPoint(point: Int, input: List<Int>): Int {
    var totalFuelCost = 0
    input.forEach {
        val dirtyConstantCost = it - point
        val cleanConstantCost = if (dirtyConstantCost > 0) {
            dirtyConstantCost
        } else {
            dirtyConstantCost * -1
        }
        totalFuelCost += getIncrementalCostFromCleanCost(cleanConstantCost)
    }
    return totalFuelCost
}

fun getIncrementalCostFromCleanCost(constantCost: Int): Int {
    var incrementalCost = 0
    for (i in 0..constantCost) {
        incrementalCost += i
    }
    return incrementalCost
}